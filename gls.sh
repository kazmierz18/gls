#!/bin/bash
# =========================================================================================
# definitions of variables, and functions
VERSION=0.2
# default values
POSITIONAL_ARGS=()
PROGRAM_NAME=$0

print_help()
{
    echo "Program description: get git status of all repos in given dir"
    echo Usage:
    echo "$PROGRAM_NAME [PATH] [OPTIONS]"
    echo -e "\tPATH                  :Path to dir that should be listed, default current directory"
    echo Options:
    echo -e "\t-l, --long            : Use long listing, prints more information"
    echo -e "\t-u, --update          : Pull for each clean repository to get latest changes"
    echo -e "\t-h, --help            : this help"
    echo -e "\t-v, --version         : version"
}

# =========================================================================================
# parse input arguments

while [[ $# -gt 0 ]]; do
    case $1 in
    -h|--help)
    print_help
    exit 0
    ;;
    -v|--version)
    echo "Version $VERSION"
    exit 0
    ;;
    #------------------------------------------------------------------
    -l|--long)
    LONG_FORMAT=1
    shift
    ;;
    #------------------------------------------------------------------
    -u|--update)
    echo "Update is not implemented yet"
    shift
    exit 1
    ;;
    #------------------------------------------------------------------
    -*|--*)
    echo "ERROR: Unknown option $1"
    echo "INFO: see --help or -h for info how to use it"
    exit 1
    ;;
    #------------------------------------------------------------------ *)
    *)
    POSITIONAL_ARGS+=("$1") # save positional args
    shift # past argument
    ;;
    esac
done

if [ "${#POSITIONAL_ARGS[@]}" -ge 2 ]; then
  echo "ERROR: Only one positional argument is expected, see --help"
  exit 1
fi
if [ "${#POSITIONAL_ARGS[@]}" -ge 1 ]; then
    WORKING_DIRECTORY=${POSITIONAL_ARGS[0]}
else
  WORKING_DIRECTORY="."
fi
RED='\033[0;31m'
GREEN='\033[0;32m'
LIGHT_GREEN='\033[1;32m'
LIGHT_BLUE='\033[1;34m'
NC='\033[0m' # No Color


if [ $# -eq 1 ]; then
    WORKING_DIRECTORY=$1
fi

DIRECTORY_CONTENT=$(ls $WORKING_DIRECTORY -1)
for ITEM in $DIRECTORY_CONTENT
do
    FULL_PATH=$(echo $WORKING_DIRECTORY/$ITEM)
    if [ -d "$FULL_PATH" ]; then
        # if this is a directory
        printf "${LIGHT_BLUE}$ITEM${NC}"
        if [ -d "$FULL_PATH/.git" ]; then
            #BRANCH=$(git --git-dir=$FULL_PATH/.git symbolic-ref -q --short HEAD)
            BRANCH=$(git -C $FULL_PATH symbolic-ref -q --short HEAD)
            #STATUS=$(git --git-dir=$FULL_PATH/.git status -s)
            STATUS=$(git -C $FULL_PATH status -s)
            if [[ -z $STATUS ]]; then 
              # Directory is clean
              printf " ${GREEN}(${BRANCH}) ${NC}"
            else 
              # Directory is dirty
              if [[ ! -z $LONG_FORMAT ]]; then
                MODIFIED=$(git -C $FULL_PATH status --short | grep "M" | wc -l)
                UNTRACKED=$(git -C $FULL_PATH status --short | grep "??" | wc -l)
                SHORT_STAT=$(printf "M:$MODIFIED ??:$UNTRACKED  ")
                printf " ${RED}(${BRANCH}): $SHORT_STAT${NC}"
              else
                printf " ${RED}(${BRANCH})"
              fi
            fi
            if [[ ! -z $LONG_FORMAT ]]; then
              # For long format append origin
              ORIGIN=$(git -C $FULL_PATH config --get remote.origin.url)
              printf "${ORIGIN}"
            fi
        fi
    else
        # this is a file
        printf "${LIGHT_GREEN}$ITEM${NC}"
    fi
    printf "\n" 
done

